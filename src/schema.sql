create table "Role"
(
    role_id smallserial
        primary key,
    name    varchar(1) not null
);

alter table "Role"
    owner to postgres;

create table "User"
(
    user_id    bigserial
        primary key,
    first_name varchar(50)  not null,
    last_name  varchar(50)  not null,
    username   varchar(256) not null
        unique,
    profession varchar(350),
    email      varchar(256) not null
        unique
        constraint "User_email_check"
            check ((email)::text ~ '^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$'::text),
    image_url  varchar(2048),
    deleted    boolean      not null,
    role_id    smallint     not null
        references "Role"
            on delete cascade
);

alter table "User"
    owner to postgres;

create index "ix_User_role_id"
    on "User" (role_id);

create table "System_permission"
(
    system_permission_id smallserial
        primary key,
    system_entity        smallint not null,
    read_access          boolean  not null,
    write_access         boolean  not null,
    execution_access     boolean  not null
);

alter table "System_permission"
    owner to postgres;

create table "Roles_to_system_permissions"
(
    role_id              smallint not null
        references "Role"
            on delete cascade,
    system_permission_id smallint not null
        references "System_permission"
            on delete cascade,
    primary key (role_id, system_permission_id)
);

alter table "Roles_to_system_permissions"
    owner to postgres;

create table "Category_type"
(
    category_type_id smallserial
        primary key,
    name             varchar(50) not null
);

alter table "Category_type"
    owner to postgres;

create table "Category"
(
    category_id smallserial
        primary key,
    name        varchar(30) not null,
    type_id     smallint    not null
        references "Category_type"
            on delete cascade,
    parent_id   smallint
                            references "Category"
                                on delete set null,
    archived    boolean     not null,
    created_at  timestamp with time zone default now()
);

alter table "Category"
    owner to postgres;

create index "ix_Category_type_id"
    on "Category" (type_id);

create index "ix_Category_parent_id"
    on "Category" (parent_id);

create table "CASE_tool"
(
    case_tool_id serial
        primary key,
    name         varchar(30) not null
        unique,
    description  varchar(100),
    category_id  smallint    not null
        references "Category"
            on delete cascade,
    author_id    bigint      not null
        references "User"
            on delete cascade,
    archived     boolean     not null,
    created_at   timestamp with time zone default now()
);

alter table "CASE_tool"
    owner to postgres;

create index "ix_CASE_tool_category_id"
    on "CASE_tool" (category_id);

create index "ix_CASE_tool_author_id"
    on "CASE_tool" (author_id);

create table "Tag"
(
    tag_id      serial
        primary key,
    name        varchar(20) not null
        unique,
    description varchar(100)
);

alter table "Tag"
    owner to postgres;

create table "CASE_tool_to_tags"
(
    case_tool_id integer not null
        references "CASE_tool"
            on delete cascade,
    tag_id       integer not null
        references "Tag"
            on delete cascade,
    primary key (case_tool_id, tag_id)
);

alter table "CASE_tool_to_tags"
    owner to postgres;

create table "Comment"
(
    comment_id   bigserial
        primary key,
    text         text    not null,
    author_id    bigint  not null
        references "User"
            on delete cascade,
    case_tool_id integer not null
        references "CASE_tool"
            on delete cascade,
    created_at   timestamp with time zone default now()
);

alter table "Comment"
    owner to postgres;

create index "ix_Comment_case_tool_id"
    on "Comment" (case_tool_id);

create index "ix_Comment_author_id"
    on "Comment" (author_id);

create table "Rating"
(
    author_id            bigint   not null
        references "User"
            on delete cascade,
    case_tool_id         integer  not null
        references "CASE_tool"
            on delete cascade,
    text                 text     not null,
    convenience_rating   smallint not null
        constraint "Rating_convenience_rating_check"
            check ((convenience_rating >= 0) AND (convenience_rating <= 10)),
    completeness_rating  smallint not null
        constraint "Rating_completeness_rating_check"
            check ((completeness_rating >= 0) AND (completeness_rating <= 10)),
    comprehensive_rating smallint not null
        constraint "Rating_comprehensive_rating_check"
            check ((comprehensive_rating >= 0) AND (comprehensive_rating <= 10)),
    created_at           timestamp with time zone default now(),
    primary key (author_id, case_tool_id)
);

alter table "Rating"
    owner to postgres;

create table "Additional_info_template"
(
    additional_info_template_id serial
        primary key,
    name                        varchar(50) not null,
    description                 varchar(100),
    default_value               varchar(200)
);

alter table "Additional_info_template"
    owner to postgres;

create table "Additional_info_templates_to_categories"
(
    additional_info_template_id integer  not null
        constraint "Additional_info_templates_to_c_additional_info_template_id_fkey"
            references "Additional_info_template"
            on delete cascade,
    category_id                 smallint not null
        references "Category"
            on delete cascade,
    primary key (additional_info_template_id, category_id)
);

alter table "Additional_info_templates_to_categories"
    owner to postgres;

create table "Additional_info"
(
    additional_info_id serial
        primary key,
    name               varchar(50)  not null,
    value              varchar(200) not null,
    parent_id          integer
                                    references "Additional_info_template"
                                        on delete set null,
    case_tool_id       integer      not null
        references "CASE_tool"
            on delete cascade
);

alter table "Additional_info"
    owner to postgres;

create index "ix_Additional_info_parent_id"
    on "Additional_info" (parent_id);

create index "ix_Additional_info_case_tool_id"
    on "Additional_info" (case_tool_id);

