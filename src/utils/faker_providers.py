from faker.providers import BaseProvider


class DefaultProvider(BaseProvider):
    _role_names = [
        'U',
        'C',
        'M',
        'A'
    ]

    def role_names(self) -> list[str]:
        return self._role_names

    def random_system_entity(self) -> int:
        return self.random_int(0, 10)
