from db import PostgreSQLConnection, Base, User, Role, SystemPermission, RolesToSystemPermissions, CategoryType, \
    Category, CASETool, Tag, CASEToolsToTags, Comment, Rating, AdditionalInfoTemplate, \
    AdditionalInfoTemplatesToCategories, AdditionalInfo
from faker import Factory
from utils import DefaultProvider


class DbInteractions:

    def __init__(self, host, port, user, password, db_name, rebuild_db=False):
        self.connection = PostgreSQLConnection(
            host=host,
            port=port,
            user=user,
            password=password,
            db_name=db_name,
            rebuild_db=rebuild_db
        )

        self.engine = self.connection.connection.engine
        self.fake_ru = Factory.create('ru_RU')
        self.fake_ru.add_provider(DefaultProvider)
        self.fake_en = Factory.create('en_PH')
        self.fake_en.add_provider(DefaultProvider)

    def recreate_tables(self):
        for table in Base.metadata.tables.keys():
            self.connection.execute_query(f'DROP TABLE IF EXISTS "{table}" CASCADE')
            Base.metadata.tables[f'{table}'].create(self.engine)

    def add_some_roles(self):
        for role_name in self.fake_ru.role_names():
            role_instance = Role(
                name=role_name
            )
            self.connection.session.add(role_instance)
        self.connection.session.commit()

    def add_some_system_permissions(self, count):
        for _ in range(count):
            system_permission_instance = SystemPermission(
                system_entity=self.fake_ru.random_system_entity(),
                read_access=self.fake_ru.random.choice([True, False]),
                write_access=self.fake_ru.random.choice([True, False]),
                execution_access=self.fake_ru.random.choice([True, False])
            )
            self.connection.session.add(system_permission_instance)
        self.connection.session.commit()

    def add_some_roles_to_system_permissions(self):
        for role_id in range(1, self.connection.session.query(Role).count() + 1):
            for system_permission_id in range(1, self.connection.session.query(SystemPermission).count() + 1):
                roles_to_system_permissions_instance = RolesToSystemPermissions(
                    role_id=role_id,
                    system_permission_id=system_permission_id
                )
                self.connection.session.add(roles_to_system_permissions_instance)
        self.connection.session.commit()

    def add_some_user_instances(self, count):
        for _ in range(count):
            profile = self.fake_ru.profile()
            if profile['sex'] == 'F':
                first_name = self.fake_ru.first_name_female()
                last_name = self.fake_ru.last_name_female()
            else:
                first_name = self.fake_ru.first_name_male()
                last_name = self.fake_ru.last_name_male()
            user_instance = User(
                first_name=first_name,
                last_name=last_name,
                username=profile['username'] + ''.join(self.fake_ru.random_letters(length=3)).lower(),
                profession=profile['job'],
                email=''.join(self.fake_ru.random_letters(length=3)).lower() + profile['mail'],
                image_url=self.fake_ru.image_url(),
                role_id=self.fake_ru.random.randint(1, self.connection.session.query(Role).count())
            )
            self.connection.session.add(user_instance)
        self.connection.session.commit()

    def add_some_category_types(self, count):
        for _ in range(count):
            category_type_instance = CategoryType(
                name=self.fake_en.company_type(),
            )
            self.connection.session.add(category_type_instance)
        self.connection.session.commit()

    def add_some_categories(self, count):
        for _ in range(count):
            category_instance = Category(
                name=self.fake_en.company()[:29],
                type_id=self.fake_ru.random.randint(1, self.connection.session.query(CategoryType).count()),
            )
            if self.fake_ru.random.choice([True, False]) and self.connection.session.query(Category).count() != 0:
                category_instance.parent_id = self.fake_ru.random.randint(1,
                                                                          self.connection.session.query(
                                                                              Category).count())
            self.connection.session.add(category_instance)
        self.connection.session.commit()

    def add_some_case_tools(self, count):
        for _ in range(count):
            case_tool_instance = CASETool(
                name=self.fake_en.hostname()[0:26] + f".{''.join(self.fake_en.random_letters(length=3)).lower()}",
                category_id=self.fake_ru.random.randint(1, self.connection.session.query(Category).count()),
                author_id=self.fake_ru.random.randint(1, self.connection.session.query(User).count()),
            )
            if self.fake_ru.random.choice([True, False]):
                case_tool_instance.description = self.fake_ru.text(max_nb_chars=100)
            self.connection.session.add(case_tool_instance)
        self.connection.session.commit()

    def add_some_tags(self, count):
        for _ in range(count):
            tag_instance = Tag(
                name=self.fake_ru.word() + f".{''.join(self.fake_ru.random_letters(length=3)).lower()}"
            )
            if self.fake_ru.random.choice([True, False]):
                tag_instance.description = self.fake_ru.text(max_nb_chars=100)
            self.connection.session.add(tag_instance)
        self.connection.session.commit()

    def add_some_case_tools_to_tags(self):
        for case_tool_id in range(1, self.connection.session.query(CASETool).count() + 1):
            for tag_id in range(1, self.connection.session.query(Tag).count() + 1):
                case_tools_to_tags_instance = CASEToolsToTags(
                    case_tool_id=case_tool_id,
                    tag_id=tag_id
                )
                self.connection.session.add(case_tools_to_tags_instance)
        self.connection.session.commit()

    def add_some_comments(self, count):
        for _ in range(count):
            comment_instance = Comment(
                text=self.fake_ru.text(),
                author_id=self.fake_ru.random.randint(1, self.connection.session.query(User).count()),
                case_tool_id=self.fake_ru.random.randint(1, self.connection.session.query(CASETool).count())
            )
            self.connection.session.add(comment_instance)
        self.connection.session.commit()

    def add_some_ratings(self):
        for user_id in range(1, self.fake_ru.random.randint(500, self.connection.session.query(CASETool).count()) + 1):
            case_tool_id = self.fake_ru.random.randint(1, self.connection.session.query(CASETool).count())
            while True:
                if self.connection.session.query(Rating).filter_by(author_id=user_id,
                                                                   case_tool_id=case_tool_id).count() == 0:
                    break
                case_tool_id = self.fake_ru.random.randint(1, self.connection.session.query(CASETool).count())
            rating_instance = Rating(
                text=self.fake_ru.text(),
                author_id=user_id,
                case_tool_id=case_tool_id,
                convenience_rating=self.fake_ru.random.randint(0, 10),
                completeness_rating=self.fake_ru.random.randint(0, 10),
                comprehensive_rating=self.fake_ru.random.randint(0, 10),
            )
            self.connection.session.add(rating_instance)
        self.connection.session.commit()

    def add_some_additional_info_templates(self, count):
        for _ in range(count):
            additional_info_template_instance = AdditionalInfoTemplate(
                name=self.fake_ru.word()
            )
            if self.fake_ru.random.choice([True, False]):
                additional_info_template_instance.description = self.fake_ru.text(max_nb_chars=100)
                additional_info_template_instance.default_value = self.fake_ru.text(max_nb_chars=200)
            self.connection.session.add(additional_info_template_instance)
        self.connection.session.commit()

    def add_some_additional_info_templates_to_categories(self):
        for additional_info_template_id in range(1, self.connection.session.query(AdditionalInfoTemplate).count() + 1):
            for category_id in range(1, self.connection.session.query(Category).count() + 1):
                additional_info_templates_to_categories_instance = AdditionalInfoTemplatesToCategories(
                    additional_info_template_id=additional_info_template_id,
                    category_id=category_id
                )
                self.connection.session.add(additional_info_templates_to_categories_instance)
        self.connection.session.commit()

    def add_some_additional_info(self, count):
        for _ in range(count):
            additional_info_instance = AdditionalInfo(
                name=self.fake_ru.word(),
                value=self.fake_ru.text(max_nb_chars=200),
                case_tool_id=self.fake_ru.random.randint(1, self.connection.session.query(CASETool).count())
            )
            if self.fake_ru.random.choice([True, False]):
                additional_info_instance.parent_id = self.fake_ru.random.randint(1, self.connection.session.query(
                    AdditionalInfoTemplate).count())
            self.connection.session.add(additional_info_instance)
        self.connection.session.commit()
