from enum import unique

from sqlalchemy import Column, ForeignKey, VARCHAR, CheckConstraint, BIGINT, BOOLEAN, SMALLINT, DateTime, INTEGER, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql import func

Base = declarative_base()


class Role(Base):
    __tablename__ = 'Role'

    role_id = Column(SMALLINT, primary_key=True, autoincrement=True, nullable=False)
    name = Column(VARCHAR(1), nullable=False)

    def __repr__(self):
        return f'<Role #{self.role_id} name="{self.name}">'


class User(Base):
    __tablename__ = 'User'

    user_id = Column(BIGINT, primary_key=True, autoincrement=True, nullable=False)
    first_name = Column(VARCHAR(50), nullable=False)
    last_name = Column(VARCHAR(50), nullable=False)
    username = Column(VARCHAR(256), nullable=False, unique=True)
    profession = Column(VARCHAR(350), nullable=True)
    email = Column(VARCHAR(256), CheckConstraint(r"email ~ '^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$'"), nullable=False,
                   unique=True)
    image_url = Column(VARCHAR(2048), nullable=True)
    deleted = Column(BOOLEAN, nullable=False, default=False)
    role_id = Column(SMALLINT, ForeignKey('Role.role_id', ondelete='CASCADE'), nullable=False, index=True)

    role = relationship('Role', backref='users')

    def __repr__(self):
        return f'<User #{self.user_id} first_name="{self.first_name}" last_name="{self.last_name}" email="{self.email}">'


class SystemPermission(Base):
    __tablename__ = 'System_permission'

    system_permission_id = Column(SMALLINT, primary_key=True, autoincrement=True, nullable=False)
    system_entity = Column(SMALLINT, nullable=False)
    read_access = Column(BOOLEAN, nullable=False)
    write_access = Column(BOOLEAN, nullable=False)
    execution_access = Column(BOOLEAN, nullable=False)

    def __repr__(self):
        return f'<SystemPermission #{self.system_permission_id} system_entity="{self.system_entity}">'


class RolesToSystemPermissions(Base):
    __tablename__ = 'Roles_to_system_permissions'

    role_id = Column(SMALLINT, ForeignKey('Role.role_id', ondelete='CASCADE'), nullable=False, primary_key=True)
    system_permission_id = Column(SMALLINT, ForeignKey('System_permission.system_permission_id', ondelete='CASCADE'),
                                  nullable=False, primary_key=True)

    role = relationship('Role', backref='roles_to_system_permissions')
    system_permission = relationship('SystemPermission', backref='roles_to_system_permissions')

    def __repr__(self):
        return f'<RolesToSystemPermissions #{self.role_id},{self.system_permission_id}>'


class CategoryType(Base):
    __tablename__ = 'Category_type'

    category_type_id = Column(SMALLINT, primary_key=True, autoincrement=True, nullable=False)
    name = Column(VARCHAR(50), nullable=False)

    def __repr__(self):
        return f'<CategoryType #{self.category_type_id} name="{self.name}">'


class Category(Base):
    __tablename__ = 'Category'

    category_id = Column(SMALLINT, primary_key=True, autoincrement=True, nullable=False)
    name = Column(VARCHAR(30), nullable=False)
    type_id = Column(SMALLINT, ForeignKey('Category_type.category_type_id', ondelete='CASCADE'), nullable=False,
                     index=True)
    parent_id = Column(SMALLINT, ForeignKey('Category.category_id', ondelete='SET NULL'), nullable=True, index=True)
    archived = Column(BOOLEAN, nullable=False, default=False)
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    type = relationship('CategoryType', backref='categories')
    parent = relationship('Category', backref='children', remote_side=category_id)

    def __repr__(self):
        return f'<Category #{self.category_id} name="{self.name}">'


class CASETool(Base):
    __tablename__ = 'CASE_tool'

    case_tool_id = Column(INTEGER, primary_key=True, autoincrement=True, nullable=False)
    name = Column(VARCHAR(30), nullable=False, unique=True)
    description = Column(VARCHAR(100), nullable=True)
    category_id = Column(SMALLINT, ForeignKey('Category.category_id', ondelete='CASCADE'), nullable=False, index=True)
    author_id = Column(BIGINT, ForeignKey('User.user_id', ondelete='CASCADE'), nullable=False, index=True)
    archived = Column(BOOLEAN, nullable=False, default=False)
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    category = relationship('Category', backref='case_tools')
    author = relationship('User', backref='case_tools')

    def __repr__(self):
        return f'<CASETool #{self.case_tool_id} name="{self.name}">'


class Tag(Base):
    __tablename__ = 'Tag'

    tag_id = Column(INTEGER, primary_key=True, autoincrement=True, nullable=False)
    name = Column(VARCHAR(20), nullable=False, unique=True)
    description = Column(VARCHAR(100), nullable=True)

    def __repr__(self):
        return f'<Tag #{self.tag_id} name="{self.name}">'


class CASEToolsToTags(Base):
    __tablename__ = 'CASE_tools_to_tags'

    case_tool_id = Column(INTEGER, ForeignKey('CASE_tool.case_tool_id', ondelete='CASCADE'), nullable=False,
                          primary_key=True)
    tag_id = Column(INTEGER, ForeignKey('Tag.tag_id', ondelete='CASCADE'), nullable=False, primary_key=True)

    case_tool = relationship('CASETool', backref='case_tool_to_tags')
    tag = relationship('Tag', backref='case_tool_to_tags')

    def __repr__(self):
        return f'<CASEToolToTags #{self.case_tool_id},{self.tag_id}>'


class Comment(Base):
    __tablename__ = 'Comment'

    comment_id = Column(BIGINT, primary_key=True, autoincrement=True, nullable=False)
    text = Column(Text, nullable=False)
    author_id = Column(BIGINT, ForeignKey('User.user_id', ondelete='CASCADE'), nullable=False, index=True)
    case_tool_id = Column(INTEGER, ForeignKey('CASE_tool.case_tool_id', ondelete='CASCADE'), nullable=False, index=True)
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    author = relationship('User', backref='comments')
    case_tool = relationship('CASETool', backref='comments')

    def __repr__(self):
        return f'<Comment #{self.comment_id} text="{self.text}">'


class Rating(Base):
    __tablename__ = 'Rating'

    author_id = Column(BIGINT, ForeignKey('User.user_id', ondelete='CASCADE'), nullable=False, primary_key=True)
    case_tool_id = Column(INTEGER, ForeignKey('CASE_tool.case_tool_id', ondelete='CASCADE'), nullable=False,
                          primary_key=True)
    text = Column(Text, nullable=False)
    convenience_rating = Column(SMALLINT, CheckConstraint('convenience_rating >= 0 AND convenience_rating <= 10'),
                                nullable=False)
    completeness_rating = Column(SMALLINT, CheckConstraint('completeness_rating >= 0 AND completeness_rating <= 10'),
                                 nullable=False)
    comprehensive_rating = Column(SMALLINT, CheckConstraint('comprehensive_rating >= 0 AND comprehensive_rating <= 10'),
                                  nullable=False)
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    author = relationship('User', backref='ratings')
    case_tool = relationship('CASETool', backref='ratings')

    def __repr__(self):
        return f'<Rating #{self.author_id},{self.case_tool_id} text="{self.text}">'


class AdditionalInfoTemplate(Base):
    __tablename__ = 'Additional_info_template'

    additional_info_template_id = Column(INTEGER, primary_key=True, autoincrement=True, nullable=False)
    name = Column(VARCHAR(50), nullable=False)
    description = Column(VARCHAR(100), nullable=True)
    default_value = Column(VARCHAR(200), nullable=True)

    def __repr__(self):
        return f'<AdditionalInfoTemplate #{self.additional_info_template_id} name="{self.name}">'


class AdditionalInfoTemplatesToCategories(Base):
    __tablename__ = 'Additional_info_templates_to_categories'

    additional_info_template_id = Column(INTEGER, ForeignKey('Additional_info_template.additional_info_template_id',
                                                             ondelete='CASCADE'), nullable=False, primary_key=True)
    category_id = Column(SMALLINT, ForeignKey('Category.category_id', ondelete='CASCADE'), nullable=False,
                         primary_key=True)

    additional_info_template = relationship('AdditionalInfoTemplate', backref='additional_info_templates_to_categories')
    category = relationship('Category', backref='additional_info_templates_to_categories')

    def __repr__(self):
        return f'<AdditionalInfoTemplatesToCategories #{self.additional_info_template_id},{self.category_id}>'


class AdditionalInfo(Base):
    __tablename__ = 'Additional_info'

    additional_info_id = Column(INTEGER, primary_key=True, autoincrement=True, nullable=False)
    name = Column(VARCHAR(50), nullable=False)
    value = Column(VARCHAR(200), nullable=False)
    parent_id = Column(INTEGER, ForeignKey('Additional_info_template.additional_info_template_id', ondelete='SET NULL'),
                       nullable=True, index=True)
    case_tool_id = Column(INTEGER, ForeignKey('CASE_tool.case_tool_id', ondelete='CASCADE'), nullable=False, index=True)

    parent = relationship('AdditionalInfoTemplate', backref='additional_info')
    case_tool = relationship('CASETool', backref='additional_info')

    def __repr__(self):
        return f'<AdditionalInfo #{self.additional_info_id} name="{self.name}">'
