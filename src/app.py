import os

from fastapi import FastAPI
from db import DbInteractions

app = FastAPI()


@app.get("/fill_db")
async def fill_db():
    # db_interactions = DbInteractions(
    #     host=os.getenv('DB_HOST'),
    #     port='5434',
    #     user=os.getenv('POSTGRES_USER'),
    #     password=os.getenv('POSTGRES_PASSWORD'),
    #     db_name=os.getenv('POSTGRES_DB'),
    #     rebuild_db=False
    # )
    # db_interactions.recreate_tables()
    #
    # db_interactions.add_some_roles()
    # db_interactions.add_some_system_permissions(50)
    # db_interactions.add_some_roles_to_system_permissions()
    # db_interactions.add_some_user_instances(2000)
    # db_interactions.add_some_category_types(150)
    # db_interactions.add_some_categories(70)
    # db_interactions.add_some_case_tools(700)
    # db_interactions.add_some_tags(200)
    # db_interactions.add_some_case_tools_to_tags()
    # db_interactions.add_some_comments(30000)
    # db_interactions.add_some_ratings()
    # db_interactions.add_some_additional_info_templates(1000)
    # db_interactions.add_some_additional_info_templates_to_categories()
    # db_interactions.add_some_additional_info(3000)

    return {"message": "Data loaded successfully!"}
